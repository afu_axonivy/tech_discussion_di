package hello;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import adam.cards.CreditCardService;

@RestController
public class GreetingController {
	
	@Resource
	private CreditCardService cardsService;

    private static final String template = "We found following credit cards: %s!";

    @RequestMapping("/greeting")
    public Greeting greeting() {
        return new Greeting(String.format(template, cardsService.getCards()));
    }
}
