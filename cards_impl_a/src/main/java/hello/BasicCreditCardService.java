package hello;

import org.springframework.stereotype.Service;

import adam.cards.CreditCardService;


@Service
public class BasicCreditCardService implements CreditCardService{
	
	@Override
    public String getCards(){
    	return "5500 0000 0000 0004";
    }
}
