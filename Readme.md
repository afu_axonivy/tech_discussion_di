# This projects demonstrate Dependency injection with springboot.

Run bash script in terminal of your choice with _bash run_awesome.sh_ to run project with service injection from card_impl_b project.
	Or _bash run_default.sh_ to run service injection from card_impl_a.

Then go to browser and open http://localhost:8080/greeting to see different implementations being used.
