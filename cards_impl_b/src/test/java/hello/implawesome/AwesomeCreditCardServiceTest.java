package hello.implawesome;

import static org.junit.Assert.*;

import java.text.MessageFormat;
import java.util.Random;

import hello.implawesome.AwesomeCreditCardService;

import org.junit.Test;

public class AwesomeCreditCardServiceTest {

	@Test
	public void testGenerateNumber() {
		Random random = new Random();
		for (int i = 0; i < 100; i++) {
		assertTrue(AwesomeCreditCardService.generateNumber(random) < 10000);
		}
	}
	
	@Test
	public void tesGetCards(){
		AwesomeCreditCardService serv = new AwesomeCreditCardService();
		String pattern = "((\\d{4}\\s){3}\\d{4},\\s){100}";
		assertTrue(serv.getCards().matches(pattern));
	}

}
