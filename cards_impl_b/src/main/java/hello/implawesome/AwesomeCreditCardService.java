package hello.implawesome;

import java.text.MessageFormat;
import java.util.Random;

import org.springframework.stereotype.Service;

import adam.cards.CreditCardService;

@Service
public class AwesomeCreditCardService implements CreditCardService{
	
	
	public AwesomeCreditCardService(){
		this.random = new Random();
	}
	
	private Random random;
	
	@Override
    public String getCards(){
    	String cards= "";
    	for (int i = 1; i <= 100; i++) {
    		int first = generateNumber(random);
    		int second = generateNumber(random);
    		int third = generateNumber(random);
    		cards += MessageFormat.format("5500 {0,number,0000} {1,number,0000} {2,number,0000}, ", first, second, third);
    	}
    	return cards;
    }
	
	/**
	 * generate number with up to 4 digts.
	 * 
	 * @param random generator
	 * @return random 4 digit number
	 */
	static int generateNumber(Random random){
		return Math.abs(random.nextInt()) % 10000;
	}
}
