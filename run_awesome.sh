#!/usr/bin/env bash

# compile projects
projects=(cards cards_impl_b)
for p in ${projects[@]}; do
  cd $p
  mvn clean install
  cd ..
done
cd spring_rest_example
mvn -Pawesome spring-boot:run
